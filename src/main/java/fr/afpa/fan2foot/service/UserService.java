package fr.afpa.fan2foot.service;

import fr.afpa.fan2foot.entity.UserEntity;
import fr.afpa.fan2foot.enums.ROLE;
import fr.afpa.fan2foot.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Service class for handling user-related operations.
 *
 * @author [Your Name]
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserService {

    // UserRepository instance for interacting with the user database
    private final UserRepository userRepository;

    /**
     * Retrieves a list of all users from the database.
     *
     * @return List of UserEntity objects representing all users in the database
     */
    public List<UserEntity> getAllUsers() {
        return userRepository.findAll();
    }

    /**
     * Retrieves a user by ID from the database.
     *
     * @param id ID of the user to retrieve
     * @return Optional of UserEntity representing the user, or an empty Optional if the user is not found
     */
    public Optional<UserEntity> getUserById(Long id) {
        return userRepository.findById(id);
    }

    /**
     * Creates a new user in the database.
     *
     * @param user UserEntity object representing the user to create
     * @return UserEntity object representing the created user
     */
    public UserEntity createUser(UserEntity user) {
        return userRepository.save(user);
    }

    /**
     * Updates an existing user in the database.
     *
     * @param user UserEntity object representing the user to update
     * @return UserEntity object representing the updated user
     */
    public UserEntity updateUser(UserEntity user) {
        return userRepository.save(user);
    }

    /**
     * Deletes a user from the database by ID.
     *
     * @param id ID of the user to delete
     */
    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }

    /**
     * Determines the appropriate redirect URL based on the user's role.
     *
     * @param user UserEntity object representing the user
     * @return String representing the redirect URL
     * @throws RuntimeException if the user's role is unknown
     */
    public String forwardingRole(UserEntity user) {
        // Check if the user's role is MANAGER
        if (user.getRole() == ROLE.MANAGER) {
            // If the user is a MANAGER, redirect to the match page
            return "redirect:/match";
            // Check if the user's role is BETTOR
        } else if (user.getRole() == ROLE.BETTOR) {
            // If the user is a BETTOR, redirect to the pari page
            return "redirect:/pari";
            // If the user's role is not MANAGER or BETTOR, throw an exception
        } else {
            throw new RuntimeException("Unknown role");
        }
    }

    public Optional<UserEntity> getUserByEmail(String email) {
        return userRepository.findUserByEmail(email);
    }
}
