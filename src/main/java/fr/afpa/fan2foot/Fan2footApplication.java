package fr.afpa.fan2foot;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Fan2footApplication implements ApplicationRunner {

	public static void main(String[] args) {
		SpringApplication.run(Fan2footApplication.class, args);
	}

	/**
	 * Méthode exécutée au démarrage de l'application.
	 * Affiche le lien vers Swagger dans le terminal.
	 */
	@Override
	public void run(ApplicationArguments args) throws Exception {
		// Spécifiez l'URL Swagger
		String swaggerUrl = "http://localhost:8080/swagger-ui/index.html";

		// Affiche un message dans le terminal avec le lien vers Swagger
		System.out.println("Ouvrez l'URL Swagger dans votre navigateur : " + swaggerUrl);


	}
}
