package fr.afpa.fan2foot.config;

import fr.afpa.fan2foot.entity.UserEntity;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * Service class for handling JSON Web Token (JWT) operations.
 */
@Service
public class JwtService {

    // The secret key used to sign the JWT
    private static final String SECRET_KEY = SecurityConstants.JWT_SECRET;

    /**
     * Extracts the username from a JWT.
     *
     * @param token The JWT string
     * @return The username from the JWT, or null if the JWT has expired
     */
    public String extractUserEmail(String token) {
        try {
            return extractClaim(token, Claims::getSubject);
        } catch (ExpiredJwtException e) {
            System.out.println("The JWT has expired.");
            return null;
        }
    }

    /**
     * Extracts a claim from a JWT.
     *
     * @param <T> The type of the claim
     * @param token The JWT string
     * @param claimsResolver A function that extracts the claim from the Claims object
     * @return The claim value
     */
    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    /**
     * Generates a JWT for a user.
     *
     * @param user The UserDetails object for the user
     * @return The JWT string
     */
    public String generateToken(UserEntity user) {
        return generateToken(new HashMap<>(), user);
    }

    /**
     * Generates a JWT for a user with additional claims.
     *
     * @param extraClaims A map of additional claims to include in the JWT
     * @param user The UserDetails object for the user
     * @return The JWT string
     */
    public String generateToken(Map<String, Object> extraClaims,UserEntity user) {
        extraClaims.put("id", user.getId());
        extraClaims.put("role", user.getRole().name());
  // firstName  dans le token    extraClaims.put("firstName", user.getFirstName());
  // firstName  dans le token      extraClaims.put("lastName", user.getLastName());


        return Jwts
                .builder()
                .setClaims(extraClaims)
                .setSubject(user.getEmail())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.JWT_EXPIRATION_TIME))
                .signWith(getSignInKey(), SignatureAlgorithm.HS256)
                .compact();
    }

    /**
     * Checks if a JWT is valid for a user.
     *
     * @param token The JWT string
     * @param userDetails The UserDetails object for the user
     * @return True if the JWT is valid, false otherwise
     */
    public boolean isTokenValid(String token, UserDetails userDetails) {
        final String userEmail = extractUserEmail(token);
        return (userEmail.equals(userDetails.getUsername())) && !isTokenExpired(token);
    }

    /**
     * Checks if a JWT has expired.
     *
     * @param token The JWT string
     * @return True if the JWT has expired, false otherwise
     */
    private boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    /**
     * Extracts the expiration date from a JWT.
     *
     * @param token The JWT string
     * @return The expiration date
     */
    private Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    /**
     * Extracts all claims from a JWT.
     *
     * @param token The JWT string
     * @return The Claims object
     */
    private Claims extractAllClaims(String token) {
        return Jwts
                .parserBuilder()
                .setSigningKey(getSignInKey())
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    /**
     * Gets the signing key for the JWT.
     *
     * @return The signing key.
     */
    private Key getSignInKey() {
        byte[] keyBytes = Decoders.BASE64.decode(SECRET_KEY);
        return Keys.hmacShaKeyFor(keyBytes);
    }
}
