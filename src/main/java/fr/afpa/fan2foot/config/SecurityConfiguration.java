package fr.afpa.fan2foot.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * Configuration class for Spring Security.
 */
@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfiguration {

    private final JwtAuthentificationFilter jwtAuthFilter;
    private final AuthenticationProvider authenticationProvider;

    /**
     * Configures the SecurityFilterChain.
     *
     * @param http The HttpSecurity object
     * @return The SecurityFilterChain bean
     * @throws Exception
     */
    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                // Spring documentation: Our recommendation is to use CSRF protection for any request that could be
                // processed by a browser by normal users. If you are only creating a service that is used by
                // non-browser clients, you will likely want to disable CSRF protection.
                // More info on CSRF protection with Spring: <https://www.baeldung.com/spring-security-csrf>
                .csrf()
                .disable()
                // When creating a user or logging in, we don't need a token, so we must whitelist these two
                // controllers to allow access
                .authorizeHttpRequests()
                // List of URLs of controllers to whitelist (** -> here means all URLs starting with
                // "/auth/" i.e. all endpoints of our AuthentificationController)
                .requestMatchers("/auth/register", "/auth/login","/auth/exists/**")
                .permitAll()
                // Any request to other controllers needs a token
                .anyRequest()
                .authenticated()
                .and()
                // Configuration of session management
                .sessionManagement()
                // Definition of how we want to create our session (here stateless because we want to create
                // a new session for each request)
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                // Definition of the authentication provider
                .authenticationProvider(authenticationProvider)
                // Adding our filter before the username and password authentication filter.
                .addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class);

        return http.build();
    }
}
