package fr.afpa.fan2foot.config;

import fr.afpa.fan2foot.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Configuration class for the application.
 * Contains the configuration for Spring Security, including the UserDetailsService, AuthenticationProvider,
 * AuthenticationManager and PasswordEncoder.
 */
@Configuration
@RequiredArgsConstructor
public class ApplicationConfig {

    private final UserRepository userRepository;



    /**
     * Bean for the UserDetailsService.
     * Retrieves a UserDetails object from the database based on the provided username (email).
     *
     * @return The UserDetailsService bean
     */
    @Bean
    public UserDetailsService userDetailsService() {
        return username -> (UserDetails) userRepository.findUserByEmail(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }

    /**
     * Bean for the AuthenticationProvider.
     * Configures the DaoAuthenticationProvider with the UserDetailsService and PasswordEncoder.
     *
     * @return The AuthenticationProvider bean
     */
    @Bean
    public AuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        // we declare the service to use to retrieve the user details
        authProvider.setUserDetailsService(userDetailsService());
        // we declare our password encoder
        authProvider.setPasswordEncoder(passwordEncoder());

        return authProvider;
    }

    /**
     * Bean for the AuthenticationManager.
     * Retrieves the AuthenticationManager bean from the AuthenticationConfiguration.
     *
     * @param config The AuthenticationConfiguration object
     * @return The AuthenticationManager bean
     * @throws Exception
     */
    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration config) throws Exception {
        return config.getAuthenticationManager();
    }

    /**
     * Bean for the PasswordEncoder.
     * Configures the BCryptPasswordEncoder.
     *
     * @return The PasswordEncoder bean
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


}
