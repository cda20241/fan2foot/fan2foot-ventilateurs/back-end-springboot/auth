package fr.afpa.fan2foot.config;

/**
 * Class containing security-related constants.
 */
public class SecurityConstants {

    /**
     * The expiration time of a JWT token in milliseconds (1 day).
     */
    public static final Integer JWT_EXPIRATION_TIME = 86400000;

    /**
     * The secret key used to sign JWT tokens.
     */
    static final String JWT_SECRET = "404E635266556A586E3272357538782F413F4428472B4B6250645367566B5970";

    /**
     * The minimum length of a user's first name.
     */
    public static final Integer MINIMUM_FIRSTNAME_LENGTH = 3;

    /**
     * The minimum length of a user's last name.
     */
    public static final Integer MINIMUM_LASTNAME_LENGTH = 3;
}
