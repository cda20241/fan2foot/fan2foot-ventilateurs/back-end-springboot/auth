package fr.afpa.fan2foot.auth;
import fr.afpa.fan2foot.entity.UserEntity;

import fr.afpa.fan2foot.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import fr.afpa.fan2foot.repository.UserRepository;
import java.util.List;
import java.util.Optional;

/**
 * Controller class for handling user authentication and registration requests.
 */
@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthentificationController {

    // This is the AuthentificationService instance that will be used to handle authentication logic
    private final AuthentificationService authentificationService;
    private final UserService userService;
    private final UserRepository userRepository;

    /**
     * Register a new user.
     *
     * @param request The registration request containing the user data.
     * @return A response containing the generated JWT token if the registration is successful,
     *         an error response otherwise.
     */
    @PostMapping("/register")
    private ResponseEntity<ResponseEntity<?>> register(@RequestBody RegisterRequest request) {
                ResponseEntity<?> response = authentificationService.register(request);
                return ResponseEntity.ok(response);
        }

    /**
     * Authenticate a user.
     *
     * @param request The authentication request containing the user's email and password.
     * @return A response containing the generated JWT token if the authentication is successful,
     *         an error response otherwise.
     */

    @PostMapping("/login")
    private ResponseEntity<ResponseEntity<?>> authentificate(@RequestBody AuthentificationRequest request) {
        ResponseEntity<?> response = authentificationService.authentificate(request);
        return ResponseEntity.ok(response);
    }

    /**
     * Get all users.
     *
     * @return A response containing a list of all users.
     */
    @GetMapping("authusers")
    private ResponseEntity<List<UserEntity>> getAllUsersFromAuthController() {
        List<UserEntity> users = userService.getAllUsers();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    /**
     * Check if a user with the provided email exists.
     *
     * @param email The email to check.
     * @return A response containing a boolean indicating whether a user with the provided email exists.
     */

    @GetMapping("exists/{email}")
    public ResponseEntity<Boolean> userExists(@PathVariable String email) {
        boolean exists = userRepository.existsByEmail(email);
        return ResponseEntity.ok(exists);
    }

}
