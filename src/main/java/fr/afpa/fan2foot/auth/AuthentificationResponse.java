package fr.afpa.fan2foot.auth;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This class represents a response to a user authentication request.
 * It contains a token that can be used to authenticate subsequent requests.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AuthentificationResponse {

    /**
    * The token that can be used to authenticate subsequent requests.
    * */
    private String token;
    private String role;
    private String fistName;
    private String lastName;


}
