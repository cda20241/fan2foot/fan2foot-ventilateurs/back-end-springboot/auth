package fr.afpa.fan2foot.auth;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This class represents a request for user authentication.
 * It contains the user's email and password.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AuthentificationRequest {

    /**
     * The email address of the user.
     */
    private String email;
    /**
     * The password of the user.
     */
    private String password;
}
