package fr.afpa.fan2foot.auth;

import fr.afpa.fan2foot.config.JwtService;
import fr.afpa.fan2foot.config.SecurityConstants;
import fr.afpa.fan2foot.entity.UserEntity;
import fr.afpa.fan2foot.exception.AuthenticationErrorResponse;
import fr.afpa.fan2foot.repository.UserRepository;
import fr.afpa.fan2foot.enums.ROLE;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Service
@RequiredArgsConstructor
public class AuthentificationService {

    // Injection of the UserRepository
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    /**
     * Check if a user with the provided email already exists in the database.
     *
     * @param request The registration request containing the email to check.
     * @return A response indicating that the email is already in use if a user with the provided email exists,
     *         null otherwise.
     */
    public ResponseEntity<?> userExistChecker(RegisterRequest request) {
        var existingUser = userRepository.findUserByEmail(request.getEmail());

        if (existingUser.isPresent()) {
            AuthenticationErrorResponse errorResponse = new AuthenticationErrorResponse("EMAIL_ALREADY_EXISTS", "Email already exists");
            return ResponseEntity.badRequest().body(errorResponse);
        }

        return null;
    }


    /**
     * Check if the provided password is null or empty.
     *
     * @param password The password to check.
     * @return A response indicating that the password cannot be empty if the provided password is null or empty,
     *         null otherwise.
     */
    public ResponseEntity<?> nullPasswordChecker(String password) {
        if (password == null || password.isEmpty()) {
            AuthenticationErrorResponse errorResponse = new AuthenticationErrorResponse("INVALID_PASSWORD", "Password cannot be empty");
            return ResponseEntity.badRequest().body(errorResponse);
        }

        return null;
    }


    /**
     * Check if the provided email is null or empty.
     *
     * @param email The email to check.
     * @return A response indicating that the email cannot be empty or null if the provided email is null or empty,
     *         null otherwise.
     */
    public ResponseEntity<?> nullEmailChecker(String email) {
        if (email == null || email.isEmpty()) {
            AuthenticationErrorResponse errorResponse = new AuthenticationErrorResponse("INVALID_EMAIL", "Email cannot be empty or null");
            return ResponseEntity.badRequest().body(errorResponse);
        }
        return null;
    }

    /**
     * Check if the provided email matches the standard email format.
     *
     * @param email The email to check.
     * @return A response indicating that the email format is invalid if the provided email does not match the standard email format,
     *         null otherwise.
     */
    public ResponseEntity<?> emailCheckerForm(String email) {

        // Format email checker
        if (!email.matches("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$")) {
            AuthenticationErrorResponse errorResponse = new AuthenticationErrorResponse("INVALID_EMAIL_FORMAT", "Invalid email format");
            return ResponseEntity.badRequest().body(errorResponse);
        }

        return null;
    }

    /**
     * Check if the provided password meets the complexity requirements.
     *
     * @param request The registration request containing the password to check.
     * @return A response indicating the specific requirement that the password does not meet if the provided password does not meet the complexity requirements,
     *         null otherwise.
     */
    public ResponseEntity<?> passwordCheckerForm(RegisterRequest request) {
        var userPassword = request.getPassword();
        boolean hasLowercase = false;
        boolean hasUppercase = false;
        boolean hasDigit = false;
        boolean hasSpecialChar = false;
        boolean hasConsecutiveChars = false;

        for (int i = 0; i < userPassword.length(); i++) {
            char c = userPassword.charAt(i);
            if (Character.isLowerCase(c)) {
                hasLowercase = true;
            } else if (Character.isUpperCase(c)) {
                hasUppercase = true;
            } else if (Character.isDigit(c)) {
                hasDigit = true;
            } else if ("@$!%*?&".indexOf(c) != -1) {
                hasSpecialChar = true;
            }

            if (i < userPassword.length() - 2) {
                char nextChar = userPassword.charAt(i + 1);
                char nextNextChar = userPassword.charAt(i + 2);

                if (c == nextChar - 1 && nextChar == nextNextChar - 1) {
                    hasConsecutiveChars = true;
                } else if (c == nextChar + 1 && nextChar == nextNextChar + 1) {
                    hasConsecutiveChars = true;
                } else if (Character.isAlphabetic(c) && Character.isAlphabetic(nextChar) && Character.isAlphabetic(nextNextChar) && (Math.abs(c - nextChar) == 1 && Math.abs(nextChar - nextNextChar) == 1)) {
                    hasConsecutiveChars = true;
                }
            }
        }

        if (!hasLowercase) {
            AuthenticationErrorResponse errorResponse = new AuthenticationErrorResponse("INVALID_PASSWORD", "Password must contain at least one lowercase letter");
            return ResponseEntity.badRequest().body(errorResponse);
        }

        if (!hasUppercase) {
            AuthenticationErrorResponse errorResponse = new AuthenticationErrorResponse("INVALID_PASSWORD", "Password must contain at least one uppercase letter");
            return ResponseEntity.badRequest().body(errorResponse);
        }

        if (!hasDigit) {
            AuthenticationErrorResponse errorResponse = new AuthenticationErrorResponse("INVALID_PASSWORD", "Password must contain at least one digit");
            return ResponseEntity.badRequest().body(errorResponse);
        }

        if (!hasSpecialChar) {
            AuthenticationErrorResponse errorResponse = new AuthenticationErrorResponse("INVALID_PASSWORD", "Password must contain at least one special character (@$!%*?&)");
            return ResponseEntity.badRequest().body(errorResponse);
        }

        if (hasConsecutiveChars) {
            AuthenticationErrorResponse errorResponse = new AuthenticationErrorResponse("INVALID_PASSWORD", "Password must not contain consecutive characters (e.g. 123, abc, qwerty)");
            return ResponseEntity.badRequest().body(errorResponse);
        }

        if (userPassword.length() < 8) {
            AuthenticationErrorResponse errorResponse = new AuthenticationErrorResponse("INVALID_PASSWORD", "Password must be at least 8 characters long");
            return ResponseEntity.badRequest().body(errorResponse);
        }


        return null;
    }
    /**

     Check if the provided first name is null, empty, or shorter than the minimum allowed length.
     @param request The registration request containing the first name to check.
     @return A response indicating the issue with the first name if the provided first name is null, empty, or shorter than the minimum allowed length,
     null otherwise. */

    public ResponseEntity<?> firstNameCheckerForm(RegisterRequest request) {
        var userFirstName = request.getFirstName();
        if (userFirstName == null || userFirstName.isEmpty()) {
            AuthenticationErrorResponse errorResponse = new AuthenticationErrorResponse("INVALID_FIRSTNAME", "firstName cannot be empty");
            return ResponseEntity.badRequest().body(errorResponse);
        }

        if (userFirstName.length() < SecurityConstants.MINIMUM_FIRSTNAME_LENGTH) {
            AuthenticationErrorResponse errorResponse = new AuthenticationErrorResponse("INVALID_FIRSTNAME", "firstName must be at least 3 characters long");
            return ResponseEntity.badRequest().body(errorResponse);
        }

        return null;
    }
    /**
     * Check if the provided last name is null, empty, or shorter than the minimum allowed length.
     *
     * @param request The registration request containing the last name to check.
     * @return A response indicating the issue with the last name if the provided last name is null, empty, or shorter than the minimum allowed length,
     *         null otherwise.
     */
    public ResponseEntity<?> lastNameCheckerForm(RegisterRequest request) {
        var userLastName = request.getLastName();

        if (userLastName == null || userLastName.isEmpty()) {
            AuthenticationErrorResponse errorResponse = new AuthenticationErrorResponse("INVALID_LASTNAME", "lastName cannot be empty");
            return ResponseEntity.badRequest().body(errorResponse);
        }


        if (userLastName.length() < SecurityConstants.MINIMUM_LASTNAME_LENGTH) {
            AuthenticationErrorResponse errorResponse = new AuthenticationErrorResponse("INVALID_LASTNAME", "lastName must be at least 3 characters long");
            return ResponseEntity.badRequest().body(errorResponse);
        }

        return null;
    }

    /**
     * Register a new user.
     *
     * @param request The registration request containing the user data.
     * @return A response containing the generated JWT token if the registration is successful,
     *         an error response otherwise.
     */
    @PostMapping("/register")
    ResponseEntity<?> register(@RequestBody RegisterRequest request) {
        var userEmail = request.getEmail(); // Save the email address from the request
        var userPassword = request.getPassword(); // Save the password from the request
        var userRole = request.getRole(); // Save the role from the request
        var userFirstName = request.getFirstName(); // Save the firstName from the request
        var userLastName = request.getLastName(); // Save the lastName from the request

        try {
            ResponseEntity<?> response = userExistChecker(request);
            if (response != null) {
                return response;
            }
            response = nullPasswordChecker(request.getPassword());
            if (response != null) {
                return response;
            }
            response = nullEmailChecker(request.getPassword());
            if (response != null) {
                return response;
            }
            response = emailCheckerForm(request.getEmail());
            if (response != null) {
                return response;
            }
            response = firstNameCheckerForm(request);
            if (response != null) {
                return response;
            }
            response = lastNameCheckerForm(request);
            if (response != null) {
                return response;
            }
            response = passwordCheckerForm(request);
            if (response != null) {
                return response;
            }

            if (userRole == null || (!userRole.equals("BETTOR") && !userRole.equals("MANAGER"))) {
                userRole = "BETTOR"; // If the role is null or different from BETTOR and MANAGER, we assign BETTOR by default
            }

            var user = UserEntity.builder()
                    .email(userEmail)
                    .password(passwordEncoder.encode(userPassword))
                    .role(ROLE.valueOf(userRole))
                    .firstName(userFirstName)
                    .lastName(userLastName)
                    .build();
            userRepository.save(user);
            var jwtToken = jwtService.generateToken(user);

            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", "Bearer " + jwtToken);

            return ResponseEntity.ok()
                    .headers(headers)
                             //.body(AuthentificationResponse.builder()
                        //  .token(jwtToken)
                            //    .role(user.getRole().name())
                            //    .fistName(user.getFirstName())
                            //    .lastName(user.getLastName())
                            .build()
           // )
            ;
        } catch (Exception e) {
            AuthenticationErrorResponse errorResponse = new AuthenticationErrorResponse("INTERNAL_SERVER_ERROR", "An error occurred while processing your request register");
            return ResponseEntity.internalServerError().body(errorResponse);
        }
    }


    /**

     Authenticate a user.
     @param request The authentication request containing the user's email and password.
     @return A response containing the generated JWT token if the authentication is successful,

     an error response otherwise.
     */
    public ResponseEntity<?> authentificate(AuthentificationRequest request) {
        try {
            ResponseEntity<?> response = nullEmailChecker(request.getEmail());
            if (response != null) {
                return response;
            }
            response = nullPasswordChecker(request.getPassword());
            if (response != null) {
                return response;
            }

            var userEmail = request.getEmail();

            if (!userEmail.matches("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$")) {
                AuthenticationErrorResponse errorResponse = new AuthenticationErrorResponse("INVALID_EMAIL_FORMAT", "Invalid email format");
                return ResponseEntity.badRequest().body(errorResponse);
            }

            var userPassword = request.getPassword();
            if (userPassword == null || userPassword.isEmpty()) {
                AuthenticationErrorResponse errorResponse = new AuthenticationErrorResponse("INVALID_PASSWORD", "Password cannot be empty or null");
                return ResponseEntity.badRequest().body(errorResponse);
            }

            try {
                authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(
                                request.getEmail(),
                                request.getPassword()
                        )
                );
            } catch (BadCredentialsException e) {
                AuthenticationErrorResponse errorResponse = new AuthenticationErrorResponse("INVALID_CREDENTIALS", "Invalid email or password");
                return ResponseEntity.badRequest().body(errorResponse);
            }

            var user = userRepository.findUserByEmail(request.getEmail())
                    .orElseThrow(() -> new RuntimeException("User not found"));

            var jwtToken = jwtService.generateToken(user);

            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", "Bearer " + jwtToken);

            return ResponseEntity.ok()
                    .headers(headers)
                                //.body(AuthentificationResponse.builder()
                                //    .token(jwtToken)
                                //    .role(user.getRole().name())
                                //    .fistName(user.getFirstName())
                                //    .lastName(user.getLastName())
                            .build()
                                // )
                            ;
        } catch (Exception e) {
            AuthenticationErrorResponse errorResponse = new AuthenticationErrorResponse("INTERNAL_SERVER_ERROR", "An error occurred while processing your request login");
            return ResponseEntity.internalServerError().body(errorResponse);
        }


    }
}