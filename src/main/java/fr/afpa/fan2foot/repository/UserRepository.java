package fr.afpa.fan2foot.repository;

import fr.afpa.fan2foot.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Interface for the UserRepository, extending JpaRepository to provide CRUD operations for the UserEntity.
 */
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    Optional<UserEntity> findUserByEmail(String email);


        boolean existsByEmail(String email);
    }
