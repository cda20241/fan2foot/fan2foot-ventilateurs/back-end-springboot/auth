package fr.afpa.fan2foot.controller;

import fr.afpa.fan2foot.entity.UserEntity;
import fr.afpa.fan2foot.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserController {

    // Private final instance of UserService
    private final UserService userService;

    /**
     * This method retrieves all users from the database.
     *
     * @return List of UserEntity objects
     */
    @GetMapping("/users")
    public List<UserEntity> getAllUsers() {
        return userService.getAllUsers();
    }

    /**
     * This method retrieves a user by its ID from the database.
     *
     * @param id The ID of the user to retrieve
     * @return An Optional object containing the UserEntity if found, otherwise empty
     */
    @GetMapping("/user/{id}")
    public Optional<UserEntity> getUserById(@PathVariable Long id) {
        return userService.getUserById(id);
    }

    /**
     * This method creates a new user in the database.
     *
     * @param user The UserEntity object to create
     * @return The created UserEntity object
     */
    @PostMapping("/user")
    public UserEntity createUser(@RequestBody UserEntity user) {
        return userService.createUser(user);
    }

    /**
     * This method updates an existing user in the database.
     *
     * @param user The UserEntity object to update
     * @return The updated UserEntity object
     */
    @PutMapping("/user")
    public UserEntity updateUser(@RequestBody UserEntity user) {
        return userService.updateUser(user);
    }

    /**
     * This method deletes a user from the database by its ID.
     *
     * @param id The ID of the user to delete
     */
    @DeleteMapping("/user/{id}")
    public void deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
    }

    /**
     * This method forwards based on the role of the user.
     *
     * @param userId The ID of the user
     * @return A string indicating the forwarding role
     */
    @GetMapping("/forward/{userId}")
    public String forwardBasedOnRole(@PathVariable Long userId) {
        UserEntity user = userService.getUserById(userId)
                .orElseThrow();
        return userService.forwardingRole(user);
    }
}
